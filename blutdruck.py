#!/usr/bin/env python3

import argparse
import os
import time
import datetime
import serial
import pandas
import openpyxl.styles as xst
import matplotlib.pyplot as graph

# --- definiere ein paar Namen und Werte
#Tabellen Namen
TabMessdaten    = 'Messdaten'
TabVerlauf      = 'Verlauf'

# Spalten Namen
SpalteZeitpunkt = 'Zeitpunkt'
SpalteAnzahl    = 'Anzahl'
SpalteSystole   = 'Systole'
SpalteDiastole  = 'Diastole'
SpaltePuls      = 'Puls'

# Datensatz
Systole         = 0
Diastole        = 1
Puls            = 2
Monat           = 3
Tag             = 4
Stunde          = 5
Minute          = 6
Jahr            = 7

# --- die Kommandozeile und deren Aufrufargumente
kommandozeile = argparse.ArgumentParser(prog='blutdruck', description='Lese Daten aus dem Breuer BM58 Blutdruckmessgerät')
kommandozeile.add_argument('-d', '--device', default='/dev/ttyUSB0', help='Gerätedatei zum Auslesen des Blutdruckmessgeräts')
kommandozeile.add_argument('-f', '--file', default='blutdruck.xlsx', help='Blutdruckverlauf in .xlsx Datei')
kommandozeile.add_argument('-g', '--graph', action='store_true', help='Wenn gesetzt, dann Blutdruckverlauf als Graph in .pdf Datei ausgeben')
kommandozeile.add_argument('-t', '--timeout', default=60, help='Wartezeit auf das Messgerät in Sekunden')
argumente = kommandozeile.parse_args()

# --- IO Datei
TimeOut = int(argumente.timeout)

fileisnew = False
if os.path.isfile(argumente.file):
    try:
        # wenn Datei vorhanden und wohlgestaltet dann lade Inhalt
        blutdruck = pandas.read_excel(argumente.file,
                                      sheet_name=TabMessdaten,
                                      usecols=[SpalteZeitpunkt,
                                               SpalteSystole,
                                               SpalteDiastole,
                                               SpaltePuls])
        print('Rohdatensätze von Datei gelesen      :', len(blutdruck))
    except:
        print('Fehler: Tabelle \"' + TabMessdaten + '\" in \"' + argumente.file + '\" fehlerhaft oder nicht vorhanden. Aufwiedersehen.')
        raise SystemExit
else:
    blutdruck = pandas.DataFrame(columns=[SpalteZeitpunkt,
                                          SpalteSystole,
                                          SpalteDiastole,
                                          SpaltePuls])  # lege leere Messdatentabelle an
    fileisnew = True

# --- IO Meßgerät
devdata = list()                # eine leeren Liste zum Aufnehmen der Daten vom Gerät

if argumente.device != '0':
    # warte auf das Erscheinen der seriellen USB-Schnittstelle
    if not os.path.exists(argumente.device):
        print('Verbinde das Messgerät mit dem Computer')
        counter = TimeOut
        while  not os.path.exists(argumente.device):
            print(counter, '\r', end='')
            counter -=1
            if counter < 0:
                print('Timeout: kein Meßgerät angschlossen. Aufwiedersehen.')
                raise SystemExit
            time.sleep(1)
        print('\r', end='')

    try:
        # öffne die Schnittstelle
        ser = serial.Serial(argumente.device, 4800, 8, 'N', 1, timeout=1)  # open serial port

        # teste Ansprechbarkeit des Blutdruckmessgeräts
        ser.write(b'\xAA')
        if ser.read() != b'\x55':
            print('Schalte Messgerät an - drücke \"MEM\" - wähle Nutzer U1/U2 mit \"START\" - bestätige mit \"MEM\"')
            counter = TimeOut
            ser.write(b'\xAA')
            while ser.read() != b'\x55':
                print(counter, '\r', end='')
                counter -=1
                if counter < 0:
                    print('Timeout: Meßgerät nicht ansprechbar. Aufwiedersehen.')
                    raise SystemExit
                ser.write(b'\xAA')
            print('\r', end='')

        # --- das Auslesen des Blutdruckmessgeräts und Erkennen neuer Daten
        ser.write(b'\xA4')              # lese Geräte-ID
        print('Gerät erkannt, ID =', ser.read(32).decode('utf-8'))

        ser.write(b'\xA2')              # Lese Anzahl der verfügbaren Datensätze
        response_num = int.from_bytes(ser.read(),byteorder='little')
        print('Datensätze am Gerät vorhanden        :', response_num)

        # Hinweis: die Datensätze liegen am Messgerät zeitlich invers vor
        for i in range(1,response_num+1):       # lese alle verfügbaren Datensätze
            ser.write(b'\xA3' + i.to_bytes(1,byteorder='little'))
            if ser.read() == b'\xac':           # prüfe Kopf des Datensatzes
                data = ser.read(8)
                timestamp = datetime.datetime(data[Jahr]+2000,
                                              data[Monat],
                                              data[Tag],
                                              data[Stunde],
                                              data[Minute])
                devdata.append([timestamp,
                                data[Systole]+25,
                                data[Diastole]+25,
                                data[Puls]])
        ser.close()

    except:
        print('Fehler: Schnittstelle zum Messgerät nicht verfügbar. Aufwiedersehen.')
        raise SystemExit

    print('Datensätze vom Gerät gelesen         :', len(devdata))

devdata = pandas.DataFrame(devdata, columns=[SpalteZeitpunkt,
                                             SpalteSystole,
                                             SpalteDiastole,
                                             SpaltePuls])

# stelle fest welche gelesenen Datensätze bereits in der .xlsx Datei vorhanden waren
index = ~(devdata[SpalteZeitpunkt].isin(blutdruck[SpalteZeitpunkt])
          & devdata[SpalteSystole].isin(blutdruck[SpalteSystole])
          & devdata[SpalteDiastole].isin(blutdruck[SpalteDiastole])
          & devdata[SpaltePuls].isin(blutdruck[SpaltePuls]))
devdata = devdata[index]      # filtere nur die neuen Datensätze heraus
new_length = len(devdata)
print('Datensätze als neu erkannt           :', new_length)

# -- das Aufbereiten und Schreiben der Daten
if new_length > 0 or argumente.device == '0':                   # wenn neue Datensätze vorhanden sind oder nicht vom Gerät gelesen werden soll
    blutdruck = blutdruck.merge(devdata, how='outer')           # füge alte und neue Datensätze zusammen
    blutdruck.sort_values(by=SpalteZeitpunkt, inplace=True)     # sortiere nach Zeitpunkt, vorallem die neuen Werte vom Messgerät, wo sie verkehrt sortiert sind

    # generiere das Gesamtmittel und den gemittelten Tagesverlauf
    verlauf = list()
    SpalteTag = blutdruck[SpalteZeitpunkt].dt.date              # nimm nur das Datum
    Tage = list(SpalteTag.drop_duplicates())                    # jeden Tag nur einmal

    verlauf.append(['Mittelwert',
                    round(len(blutdruck)/len(Tage), 1),
                    round(blutdruck[SpalteSystole].mean(),1),
                    round(blutdruck[SpalteDiastole].mean(),1),
                    round(blutdruck[SpaltePuls].mean(),1)])
    for i in range(len(Tage)):
        index = (Tage[i] == SpalteTag)
        tageswerte = blutdruck[[SpalteSystole, SpalteDiastole, SpaltePuls]][index]
        tagesmittel = (tageswerte.mean()).round()
        verlauf.append([Tage[i], len(tageswerte),
                        tagesmittel[SpalteSystole],
                        tagesmittel[SpalteDiastole],
                        tagesmittel[SpaltePuls]])
    verlauf = pandas.DataFrame(verlauf, columns=[SpalteZeitpunkt,
                                                 SpalteAnzahl,
                                                 SpalteSystole,
                                                 SpalteDiastole,
                                                 SpaltePuls])

    # schreibe alle Datensätze in die .xlsx Datei
    if fileisnew:
        print('Datei \"' + argumente.file + '\" wird neu angelegt.')
    try:
        writer = pandas.ExcelWriter(argumente.file)             # öffne die .xlsx Datei
    except:
        print('Fehler: Datei \"' + argumente.file + '\" ist nicht schreibbar. Aufwiedersehen.')
        raise SystemExit

    workbook = writer.book

    # schreibe und formatiere die gemittelten Tagesdatensätze
    verlauf.to_excel(writer, sheet_name = TabVerlauf, index = False)
    print('Tagesdatensätze in Datei geschrieben :', len(verlauf)-1) # der erste Datensatz beinhaltet die Gesamtmittelwerte
    worksheet = workbook[TabVerlauf]
    for timecell in worksheet['A']:
        timecell.number_format = 'dd.mm.yyyy'
    worksheet.column_dimensions['A'].width = 10

    # formattiere die Zeile mit dem Gesamtmittelwerten
    rand = xst.Border(left=xst.Side(style='thin'),
                                  right=xst.Side(style='thin'),
                                  top=xst.Side(style='thin'),
                                  bottom=xst.Side(style='double'))
    for cell in worksheet['2']:
        cell.border = rand

    worksheet['A1'].number_format = ''
    worksheet['A2'].number_format = ''
    worksheet['A2'].alignment = xst.Alignment(horizontal = 'right')

    # schreibe und formatiere die Rohdatensätze
    blutdruck.to_excel(writer, sheet_name = TabMessdaten, index = False)
    print('Rohdatensätze in Datei geschrieben   :', len(blutdruck))
    worksheet = workbook[TabMessdaten]
    for timecell in worksheet['A']:
        timecell.number_format = 'dd.mm.yyyy hh:mm'
    worksheet.column_dimensions['A'].width = 15

    writer.close()

    if argumente.graph:             # erzeuge Graphik der gemittelten Tagesdatensätze
        grafikdatei = argumente.file.replace('.xlsx', '.pdf')
        grafik = verlauf.drop(0)    # entferne die Zeile mit den Gesamtmittelwerten

        starttag = grafik[SpalteZeitpunkt].iloc[0]
        endtag = grafik[SpalteZeitpunkt].iloc[-1]
        msystole = verlauf[SpalteSystole].iloc[0]
        mdiastole = verlauf[SpalteDiastole].iloc[0]
        mpuls = verlauf[SpaltePuls].iloc[0]

        graph.figure(figsize=[11.69, 8.27], dpi=150)
        graph.suptitle('Verlauf der Tagesmittel (' + argumente.file + ')', fontsize=20)

        ax1 = graph.subplot(311)
        graph.title('Systole', fontsize=16)
        graph.plot([starttag, endtag], [msystole, msystole], 'r--', label='Langzeitmittel = ' + str(msystole) + ' mmHg')
        graph.plot(grafik[SpalteZeitpunkt], grafik[SpalteSystole], 'bo', grafik[SpalteZeitpunkt], grafik[SpalteSystole], 'b')
        graph.legend(loc='upper right')
        graph.tick_params('x', labelbottom=False)
        graph.grid(True)
        graph.ylabel('mmHg')

        graph.subplot(312, sharex=ax1)
        graph.title('Diastole', fontsize=16)
        graph.plot([starttag, endtag], [mdiastole, mdiastole], 'r--', label='Langzeitmittel = ' + str(mdiastole) + ' mmHg')
        graph.plot(grafik[SpalteZeitpunkt], grafik[SpalteDiastole], 'go', grafik[SpalteZeitpunkt], grafik[SpalteDiastole], 'g')
        graph.legend(loc='upper right')
        graph.tick_params('x', labelbottom=False)
        graph.grid(True)
        graph.ylabel('mmHg')

        graph.subplot(313, sharex=ax1)
        graph.title('Puls', fontsize=16)
        graph.plot([starttag, endtag], [mpuls, mpuls], 'r--', label='Langzeitmittel = ' + str(mpuls) + ' min⁻¹')
        graph.plot(grafik[SpalteZeitpunkt], grafik[SpaltePuls], 'yo', grafik[SpalteZeitpunkt], grafik[SpaltePuls], 'y')
        graph.legend(loc='upper right')
        graph.grid(True)
        graph.ylabel('min⁻¹')

        graph.savefig(grafikdatei, orientation = 'portrait', format = 'pdf')
        print('Blutdruckverlauf als Grafik exportiert.')

else:               # wenn keine neuen Datensätze dann lasse die .xlsx Datei unberührt
    if fileisnew:
        print('Nix zu tun.')
    else:
        print('Blutdruckverlauf in \"' + argumente.file + '\" unverändert.')

print('fertig')
