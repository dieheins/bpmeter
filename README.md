Manage Blood Pressure Data from a Beurer BM58
=============================================

# Purpose

Build and maintain an archive of your blood pressure and pulse data measured by a Beurer BM58 Blood pressure Meter.

* Fetch blood pressure and pulse data from the device
* merge them with archieved data stored in an spreadsheet file
* generate a per-day average
* store them altogether back to the spreadsheet archive
* and generate a blood pressure and pulse log over time

## Supported Device(s)

Currently only those meters are supported which incorporate the USB-Serial Controller `pl2303`. This can be checked in the system log upon plugging in.

```
...
Oct 31 11:55:00 samuin kernel: [173617.638934] usb 1-3: new full-speed USB device number 8 using xhci_hcd
Oct 31 11:55:00 samuin kernel: [173617.788046] usb 1-3: New USB device found, idVendor=067b, idProduct=2303, bcdDevice= 3.00
Oct 31 11:55:00 samuin kernel: [173617.788063] usb 1-3: New USB device strings: Mfr=1, Product=2, SerialNumber=0
Oct 31 11:55:00 samuin kernel: [173617.788070] usb 1-3: Product: USB-Serial Controller
Oct 31 11:55:00 samuin kernel: [173617.788075] usb 1-3: Manufacturer: Prolific Technology Inc.
Oct 31 11:55:00 samuin kernel: [173617.790192] pl2303 1-3:1.0: pl2303 converter detected
Oct 31 11:55:00 samuin kernel: [173617.791930] usb 1-3: pl2303 converter now attached to ttyUSB0
...
```
## Installation

Download into local repository, move to folder hosting local executables and adapt owner.

```
cd your_repository_folder
git clone https://gitlab.com/dieheins/bpmeter.git
sudo cp -a bpmeter/blutdruck.py /usr/local/bin/blutdruck
sudo chown root:root /usr/local/bin/blutdruck
```

Now you are all set.


## Python Prerequisites

Python 3.10

### Required Python Packages:

| Package              | min. Version  | max. Version  |
| :--------------------| :-----------: | :-----------: |
| `pandas`             |  1.3.5        |  t.b.d        |
| `pyserial`           |  3.5          |  t.b.d        |
| `openpyxl`           |  3.0.9        |  t.b.d        |
| `python-dateutil`    |  2.8.1        |  t.b.d        |
| `matplotlib`         |  3.5.1        |  t.b.d        |


# Usage

## Synopsis

short form:

### blutdruck [-d _DEVICE_] [-f _ARCHIVE_] [-g] [-t _TIMEOUT_]

normal form:

### blutdruck [--device=_DEVICE_] [--file=_ARCHIVE_] [--graph] [--timeout=_TIMEOUT_]


## Options & Parameters

with their default values and their meanings.

| Option          | Parameter       | default value   | Comment                                                                               |
| :-------------- | :-------------- | :-------------- | :------------------------------------------------------------------------------------ |
| -d \| --device  | `DEVICE`        |  /dev/ttyUSB0   | USB-Serial Device, if set to `0` no device will be approached                         |
| -f \| --file    | `ARCHIVE`.xlsx  |  blutdruck.xlsx | blood pressure and pulse archive in a spreadsheet file of .xlsx format                 |
| -g \| --graph   |  none           |  n.a.           | if set a daily averages log of blood pressure and pulse gets exported to `ARCHIVE`.pdf |
| -t \| --timeout | `TIMEOUT`       |  60             | time in seconds to wait for `DEVICE` to get ready                                     |


## Behaviour

If the archive file `ARCHIVE`.xlsx is not existent but data can be fetched from the meter then the file will be created and initialized with all the fetched data. That is the prefered way to setup the archive file as all tables, columns and cells are well formatted for subsequent updates.

Only data on the meter which are new to the given `ARCHIVE`.xlsx will make it into the file. Data already fetched and archived once in `ARCHIVE`.xlsx during a previous run will be ignored.

And no worries: the application will guide you through the process, telling you when to press which button on the meter.

## Archive Data Structures

The actual blood pressure and pulse data archive within the spreadsheet file `ARCHIVE`.xlsx is in the table `Messdaten`. Each time

* either new data is fetched from the meter and appended to the existing data from the `Messdaten` table
* or `DEVICE` is explicetly set to `0`

another table `Verlauf` gets re-created and saved to the spreadsheet file. The data in `Verlauf` represent the daily average of blood pressure and pulse.

### Table _Messdaten_

This table contains the measured blood pressure and pulse data in a chronological order. Each time new data is appended the whole archive gets reorderd. This allows to fetch data even from different blood pressure meters in an arbitrary chronological order.

| Zeitpunkt        | Systole | Diastole | Puls |
|:----------------:|:-------:|:--------:|:----:|
| d0.m0.yyyy h0:m0 | ss0     | d0       | p0   |
| d1.m1.yyyy h1:m1 | ss1     | d1       | p1   |
| d1.m1.yyyy h2:m2 | ss2     | d2       | p2   |
| d1.m1.yyyy h3:m3 | ss3     | d3       | p3   |
| d2.m1.yyyy h4:m4 | ss4     | d4       | p4   |
| d2.m1.yyyy h5:m5 | ss5     | d5       | p5   |
| d3.m2.yyyy h6:m6 | ss6     | d6       | p6   |
| d3.m2.yyyy h7:m7 | ss7     | d7       | p7   |
| d3.m2.yyyy h8:m8 | ss8     | d8       | p8   |
| d3.m2.yyyy h9:m9 | ss9     | d9       | p9   |


### Table _Verlauf_

Note: this table will be created out of the archiv data - any previous modifications get lost.

The first data row shows the average number of measurements per day as well as the average blood pressure and pulse data over all measured values. In all other subsequent rows the numnber of measurements per a specific day and the averages for blood pressure and pulse over all measured values of that day are given in a chronological order.
The first row's elements are rounded to the first digit after decimal point, all other rows' elements are rounded to unit digit.

| Zeitpunkt  | Anzahl | Systole       | Diastole    | Puls        |
|:----------:|:------:|:-------------:|:-----------:|:-----------:|
| Mittelwert | 2.5    | avg(ss0..ss9) | avg(d0..d9) | avg(p0..p9) |
| d0.m0.yyyy | 1      | ss0           | d0          | p0          |
| d1.m1.yyyy | 3      | avg(ss1..ss3) | avg(d1..d3) | avg(p1..p3) |
| d2.m1.yyyy | 2      | avg(ss4..ss5) | avg(d4..d5) | avg(p4..p5) |
| d3.m2.yyyy | 4      | avg(ss6..ss9) | avg(d6..d9) | avg(p6..p9) |

### Diagram in `ARCHIVE`.pdf

It only gets created out of `Verlauf` table when the option _-g_ | _--graph_ is set. It prints the curves over time and the long term average line of the systolic and diastolic blood pressure as well as the heart beat rate into a pdf graphic. The actual pdf file's name `ARCHIVE`.pdf is derived from the `ARCHIVE` file name `ARCHIVE`.xlsx.

***

# Closing

## Authors and acknowledgments

inverse mailto: ed.xmg@nieh.renrew

Many thanks to Werner Meternek at Beurer GmbH, who provided me (already in 2014) with the protocol information how to read out the BM58.

## License and Legal Disclaimer

The use of this program is free under the terms of GPLv3.

NO guarantees or warranties are made by the author in the use of this program.

Use on your own risk.
